<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view ('test-register');
    }

    public function welcome(Request $request) {
        $namadepan = $request['fnama'];
        $namabelakang = $request['lnama'];
        return view ('test-welcome', compact('namadepan', 'namabelakang'));
    }

}

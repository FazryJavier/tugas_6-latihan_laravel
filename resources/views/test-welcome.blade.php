@extends('layouts/master')

@section('judul')
Selamat Datang
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$namadepan}} {{$namabelakang}}</h1>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
@endsection
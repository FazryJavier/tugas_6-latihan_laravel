@extends('layouts/master')

@section('judul')
Halaman Registrasi
@endsection

@section('content')
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
    @csrf
    @method('POST')
        <label>First Name :</label> <br>
        <input type="text" name="fnama"> <br><br>
        <label>Last Name :</label> <br>
        <input type="text" name="lnama"> <br><br>
        <label>Gender :</label> <br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br><br>
        <label>Nationality :</label> <br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="malaysian">Malaysian</option>
            <option value="american">American</option>
            <option value="japan">Japan</option>
        </select> <br><br>
        <label>Language Spoken :</label> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Japanese <br>
        <input type="checkbox"> Others <br><br>
        <label>Bio :</label> <br>
        <textarea name="bio" cols="40" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
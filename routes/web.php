<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/home', function () {
//     return view('test-home');
// });

// Route::get('/register', function () {
//     return view('test-register');
// });

// Route::get('/welcome', function () {
//     return view('test-welcome');
// });

Route::get('/home', 'App\Http\Controllers\IndexController@home');
Route::get('/register', 'App\Http\Controllers\AuthController@register');
Route::post('/welcome', [AuthController::class, 'welcome']);
Route::get('/data-tables', 'App\Http\Controllers\IndexController@datatables');

// Route::get('/master', function() {
//     return view('/layouts/master');
// });